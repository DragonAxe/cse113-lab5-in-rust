use std::io;

#[derive(Copy, Clone, Debug)]
pub enum WeekDay {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY,
}

#[derive(Copy, Clone, Debug)]
pub struct Zombie {
    pub dead: Option<bool>,
    pub day: WeekDay,
    pub toes: i32,
    pub blood: f64,
    pub hour: i32,
    pub min: i32,
    pub sec: i32,
}

pub fn get_dead() -> bool {
    println!("Was the zombie found dead? Y or N");
    loop {
        let mut buffer = String::new();

        io::stdin()
            .read_line(&mut buffer)
            .expect("Could not read line from stdin");

        let trimmed = buffer.trim();

        if trimmed.len() == 1 {
            let yn = trimmed.chars().next().unwrap();
            if yn == 'y' || yn == 'Y' {
                println!("");
                break true;
            } else if yn == 'n' || yn == 'N' {
                println!("");
                break false;
            }
        }

        println!("Error: A zombie groan startles you and you mess up your answer, try again:");
    }
}

pub fn get_time() -> (i32, i32, i32) {
    println!("Enter time when this zombie was encountered (HH:MM:SS):");
    loop {
        let mut buffer = String::new();

        io::stdin()
            .read_line(&mut buffer)
            .expect("Could not read line from stdin");

        let nums: Vec<_> = buffer
            .split(':')
            .map(|x| x.trim())
            .map(|x| x.parse::<i32>())
            .filter(|x| x.is_ok())
            .map(|x| x.unwrap())
            .collect();

        if nums.len() == 3 {
            if nums[0] < 24 && nums[1] < 60 && nums[2] < 60 {
                if nums[0] >= 0 && nums[1] >= 0 && nums[2] >= 0 {
                    println!("");
                    break (nums[0], nums[1], nums[2]);
                }
            }
        }

        println!("Error: The time you entered is zombie like, try again:");
    }
}

pub fn get_toes() -> i32 {
    println!("Enter the number of toes the zombie has:");
    loop {
        let mut buffer = String::new();

        io::stdin()
            .read_line(&mut buffer)
            .expect("Could not read line from stdin");

        let toes = buffer.trim().parse::<i32>();

        if toes.is_ok() {
            println!("");
            break toes.unwrap();
        }

        println!("Error: The toes you entered came out all crooked, try again:");
    }
}

pub fn get_blood() -> f64 {
    println!("Enter the amount of blood that oozed from its body after you killed it (in mL):");
    loop {
        let mut buffer = String::new();

        io::stdin()
            .read_line(&mut buffer)
            .expect("Could not read line from stdin");

        let blood = buffer.trim().parse::<f64>();

        if blood.is_ok() {
            println!("");
            break blood.unwrap();
        }

        println!("Error: The blood smeared all over the paper, try again:");
    }
}

pub fn get_day_of_week() -> WeekDay {
    println!("Enter the day this zombie was encountered:");
    println!("1) Monday");
    println!("2) Tuesday");
    println!("3) Wednesday");
    println!("4) Thursday");
    println!("5) Friday");
    println!("6) Saturday");
    println!("7) Sunday");
    loop {
        let mut buffer = String::new();

        io::stdin()
            .read_line(&mut buffer)
            .expect("Could not read line from stdin");

        let weekday = buffer.trim().parse::<i32>();

        if weekday.is_ok() {
            println!("");
            match weekday.unwrap() {
                1 => break WeekDay::MONDAY,
                2 => break WeekDay::TUESDAY,
                3 => break WeekDay::WEDNESDAY,
                4 => break WeekDay::THURSDAY,
                5 => break WeekDay::FRIDAY,
                6 => break WeekDay::SATURDAY,
                7 => break WeekDay::SUNDAY,
                _ => (),
            }
        }

        println!("Error: Your hand trembles from weekness unable to write, try again:");
    }
}

pub fn print_data(zombies: &[Zombie], current: usize) {
    for zombie in zombies
        .iter()
        .cycle()
        .skip(current)
        .take(zombies.len())
        .filter(|x| x.dead.is_some())
        .enumerate()
    {
        // let zombie = zombies[i + current];
        match zombie.1.dead {
            Some(is_dead) => {
                if is_dead {
                    println!("{}. This zombie was found DEAD.", zombie.0 + 1);
                    println!("It had {} toes.", zombie.1.toes);
                } else {
                    println!("{}. This zombie was found ALIVE!", zombie.0 + 1);
                    println!(
                        "It was drained of {} mL of blood once killed.",
                        zombie.1.blood
                    );
                }
                println!(
                    "Zombie seen on {} at {:02}:{:02}:{:02}",
                    get_weekday_name(zombie.1.day),
                    zombie.1.hour,
                    zombie.1.min,
                    zombie.1.sec
                );
                println!("");
            }
            None => (),
        }
    }
    if zombies.iter().filter(|x| x.dead.is_some()).count() == 0 {
        println!("No zombies recorded.\n");
    }
}

fn get_weekday_name<'a>(weekday: WeekDay) -> &'a str {
    match weekday {
        WeekDay::MONDAY => "Monday",
        WeekDay::TUESDAY => "Tuesday",
        WeekDay::WEDNESDAY => "Wednesday",
        WeekDay::THURSDAY => "Thursday",
        WeekDay::FRIDAY => "Friday",
        WeekDay::SATURDAY => "Saturday",
        WeekDay::SUNDAY => "Sunday",
    }
}
