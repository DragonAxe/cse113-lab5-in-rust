use std::io;
mod zombie;

fn main() {
    println!("Welcome to the zombie tracker!");
    println!("");
    const MAX_ZOMBIE: usize = 5;
    let mut zombies = [zombie::Zombie {
        dead: None,
        day: (zombie::WeekDay::MONDAY),
        toes: 0,
        blood: 0.0,
        hour: 0,
        min: 0,
        sec: 0,
    }; MAX_ZOMBIE];
    let mut current = 0;

    loop {
        match get_menu() {
            1 => {
                let is_dead = zombie::get_dead();
                zombies[current].dead = Some(is_dead);
                if is_dead {
                    zombies[current].toes = zombie::get_toes()
                } else {
                    zombies[current].blood = zombie::get_blood()
                }
                zombies[current].day = zombie::get_day_of_week();
                let (hh, mm, ss) = zombie::get_time();
                zombies[current].hour = hh;
                zombies[current].min = mm;
                zombies[current].sec = ss;

                current = (current + 1) % MAX_ZOMBIE;
            }
            2 => {
                zombie::print_data(&zombies, current);
            }
            3 => {
                println!("Fair thee well traveler!");
                break;
            }
            _ => {
                println!("Error: Unknown Option, try again:");
            }
        }
    }
}

fn get_menu() -> i32 {
    println!("1) Enter new zombie");
    println!("2) Display zombies");
    println!("3) Return to fighting zombies (exit)");
    loop {
        let mut buffer = String::new();

        io::stdin()
            .read_line(&mut buffer)
            .expect("Could not read line from stdin");

        let toes = buffer.trim().parse::<i32>();

        if toes.is_ok() {
            println!("");
            break toes.unwrap();
        }

        println!("Error: You drop the zombie tracker, the screen flickers and resets, try again:");
    }
}
